CVE-2022-3650
	[bullseye] - ceph 14.2.21-1+deb11u1
CVE-2022-37026
	[bullseye] - erlang 1:23.2.6+dfsg-1+deb11u1
CVE-2021-32718
	[bullseye] - rabbitmq-server 3.8.9-3+deb11u1
CVE-2021-32719
	[bullseye] - rabbitmq-server 3.8.9-3+deb11u1
CVE-2021-22116
	[bullseye] - rabbitmq-server 3.8.9-3+deb11u1
CVE-2018-1279
	[bullseye] - rabbitmq-server 3.8.9-3+deb11u1
CVE-2021-3654
	[bullseye] - nova 2:22.2.2-1+deb11u1
CVE-2022-27240
	[bullseye] - glewlwyd 2.5.2-2+deb11u3
CVE-2022-29967
	[bullseye] - glewlwyd 2.5.2-2+deb11u3
CVE-2022-32096
	[bullseye] - rhonabwy 0.9.13-3+deb11u2
CVE-2022-28737
	[bullseye] - shim 15.6-1~deb11u1
CVE-2021-24119
	[bullseye] - mbedtls 2.16.12-0+deb11u1
CVE-2021-44732
	[bullseye] - mbedtls 2.16.12-0+deb11u1
CVE-2022-2996
	[bullseye] - python-scciclient 0.8.0-2+deb11u1
CVE-2022-42961
	[bullseye] - wolfssl 4.6.0+p1-0+deb11u2
CVE-2022-39173
	[bullseye] - wolfssl 4.6.0+p1-0+deb11u2
CVE-2022-42905
	[bullseye] - wolfssl 4.6.0+p1-0+deb11u2
CVE-2022-46146
	[bullseye] - golang-github-prometheus-exporter-toolkit 0.5.1-2+deb11u2
CVE-2022-23527
	[bullseye] - libapache2-mod-auth-openidc 2.4.9.4-0+deb11u2
CVE-2022-4415
	[bullseye] - systemd 247.3-7+deb11u2
CVE-2022-3821
	[bullseye] - systemd 247.3-7+deb11u2
CVE-2022-1227
	[bullseye] - golang-github-containers-psgo 1.5.2-2~deb11u1
CVE-2021-3468
	[bullseye] - avahi 0.8-5+deb11u2
CVE-2021-3482
	[bullseye] - exiv2 0.27.3-3+deb11u2
CVE-2021-29458
	[bullseye] - exiv2 0.27.3-3+deb11u2
CVE-2021-29463
	[bullseye] - exiv2 0.27.3-3+deb11u2
CVE-2021-29464
	[bullseye] - exiv2 0.27.3-3+deb11u2
CVE-2021-29470
	[bullseye] - exiv2 0.27.3-3+deb11u2
CVE-2021-29473
	[bullseye] - exiv2 0.27.3-3+deb11u2
CVE-2021-29623
	[bullseye] - exiv2 0.27.3-3+deb11u2
CVE-2021-32815
	[bullseye] - exiv2 0.27.3-3+deb11u2
CVE-2021-34334
	[bullseye] - exiv2 0.27.3-3+deb11u2
CVE-2021-34335
	[bullseye] - exiv2 0.27.3-3+deb11u2
CVE-2021-37615
	[bullseye] - exiv2 0.27.3-3+deb11u2
CVE-2021-37616
	[bullseye] - exiv2 0.27.3-3+deb11u2
CVE-2021-37618
	[bullseye] - exiv2 0.27.3-3+deb11u2
CVE-2021-37619
	[bullseye] - exiv2 0.27.3-3+deb11u2
CVE-2021-37620
	[bullseye] - exiv2 0.27.3-3+deb11u2
CVE-2021-37621
	[bullseye] - exiv2 0.27.3-3+deb11u2
CVE-2021-37622
	[bullseye] - exiv2 0.27.3-3+deb11u2
CVE-2021-37623
	[bullseye] - exiv2 0.27.3-3+deb11u2
CVE-2022-46175
	[bullseye] - node-json5 2.1.3-2+deb11u1
CVE-2022-24859
	[bullseye] - pypdf2 1.26.0-4+deb11u1
CVE-2022-47952
	[bullseye] - lxc 1:4.0.6-2+deb11u2
CVE-2022-22728
	[bullseye] - libapreq2 2.13-7+deb11u1
CVE-2006-20001
	[bullseye] - apache2 2.4.55-1~deb11u1
CVE-2022-36760
	[bullseye] - apache2 2.4.55-1~deb11u1
CVE-2022-37436
	[bullseye] - apache2 2.4.55-1~deb11u1
CVE-2022-38223
	[bullseye] - w3m 0.5.3+git20210102-6+deb11u1
CVE-2022-4883
	[bullseye] - libxpm 1:3.5.12-1.1~deb11u1
CVE-2022-44617
	[bullseye] - libxpm 1:3.5.12-1.1~deb11u1
CVE-2022-46285
	[bullseye] - libxpm 1:3.5.12-1.1~deb11u1
CVE-2020-36646
	[bullseye] - libzen 0.4.38-1+deb11u1
CVE-2022-48279
	[bullseye] - modsecurity-apache 2.9.3-3+deb11u2
CVE-2023-24021
	[bullseye] - modsecurity-apache 2.9.3-3+deb11u2
CVE-2022-24895
	[bullseye] - symfony 4.4.19+dfsg-2+deb11u2
CVE-2022-24894
	[bullseye] - symfony 4.4.19+dfsg-2+deb11u2
CVE-2022-29458
	[bullseye] - ncurses 6.2+20201114-2+deb11u1
